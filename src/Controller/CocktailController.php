<?php

namespace App\Controller;

use App\Entity\Cocktail;
use App\Entity\Ingrediente;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;

class CocktailController extends AbstractController {

    #[Route("/cocktail/{id}", name:"showCocktail")]
    public function showCocktail(EntityManagerInterface $doctrine, $id) {
        $repository = $doctrine -> getRepository(Cocktail::class);
        $cocktail = $repository -> find($id);

        return $this -> render("cocktails/showCocktail.html.twig", ["cocktail" => $cocktail]);
    }

    #[Route("/cocktails", name:"listCocktail")]
    public function listCocktail(EntityManagerInterface $doctrine) {
        $repository = $doctrine -> getRepository(Cocktail::class);
        $cocktails = $repository -> findAll();
        
        return $this -> render("cocktails/listCocktail.html.twig", ["cocktails" => $cocktails]);
    }

    #[Route("/new/cocktail")]
    public function newCocktail(EntityManagerInterface $doctrine ) {
        $cocktail1 = new Cocktail();
        $cocktail1 -> setName("Mojito");
        $cocktail1 -> setCategory("Cocktail");
        $cocktail1 -> setGlass("Highball glass");
        $cocktail1 -> setImage("https://www.thecocktaildb.com/images/media/drink/metwgh1606770327.jpg");
        $cocktail1 -> setIngredient1("light rum");
        $cocktail1 -> setIngredient2("lemon juice");
        $cocktail1 -> setIngredient3("2 tsp sugar");
        $cocktail1 -> setIngredient4("2-4 mint and soda water");

        $cocktail2 = new Cocktail();
        $cocktail2 -> setName("Between The Sheets");
        $cocktail2 -> setCategory("Cocktail");
        $cocktail2 -> setGlass("Classic");
        $cocktail2 -> setImage("https://www.thecocktaildb.com/images/media/drink/of1rj41504348346.jpg");
        $cocktail2 -> setIngredient1("Brandy");
        $cocktail2 -> setIngredient2("light rum");
        $cocktail2 -> setIngredient3("triple sec");
        $cocktail2 -> setIngredient4("lemon juice");

        $cocktail3 = new Cocktail();
        $cocktail3 -> setName("9 1/2 Weeks");
        $cocktail3 -> setCategory("Cocktail");
        $cocktail3 -> setGlass("Cocktail glass");
        $cocktail3 -> setImage("https://www.thecocktaildb.com/images/media/drink/xvwusr1472669302.jpg");
        $cocktail3 -> setIngredient1("Absolut citron");
        $cocktail3 -> setIngredient2("triple sec");
        $cocktail3 -> setIngredient3("strawberry liqueur");
        $cocktail3 -> setIngredient4("orange juice");

        $ingrediente1 = new Ingrediente();
        $ingrediente1 -> setPrimero("lemon juice");

        $ingrediente2 = new Ingrediente();
        $ingrediente2 -> setPrimero("light rum");

        $ingrediente3 = new Ingrediente();
        $ingrediente3 -> setPrimero("triple sec");

        $cocktail1 -> addIngrediente($ingrediente1);
        $cocktail2 -> addIngrediente($ingrediente2);
        $cocktail2 -> addIngrediente($ingrediente1);
        $cocktail3 -> addIngrediente($ingrediente3);


        $doctrine -> persist($cocktail1);
        $doctrine -> persist($cocktail2);
        $doctrine -> persist($cocktail3);

        $doctrine -> persist($ingrediente1);
        $doctrine -> persist($ingrediente2);
        $doctrine -> persist($ingrediente3);

        $doctrine -> flush();

        return new Response("Cocktail añadido correctamente");
    }

}